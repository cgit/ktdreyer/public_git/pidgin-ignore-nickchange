%global commitdate 20160905
%global commit c0b3e4a15e484ec5f8518e3ebbfb246cc31b78af
%global shortcommit %(c=%{commit}; echo ${c:0:7})

Name:           pidgin-ignore-nickchange
Version:        0
Release:        0.1.%{commitdate}git%{shortcommit}%{?dist}
Summary:        Pidgin plugin to ignore "X is now known as Y" messages in chat.
Group:          Applications/Internet
License:        GPLv2+
URL:            https://github.com/EionRobb/pidgin-ignore-nickchange
Source0:        https://github.com/EionRobb/%{name}/archive/%{commit}/%{name}-%{version}-%{shortcommit}.tar.gz

BuildRequires:  pkgconfig(glib-2.0) >= 2.12.0
BuildRequires:  pkgconfig(pidgin)
BuildRequires:  pkgconfig(purple)

%description
Pidgin plugin to ignore "X is now known as Y" messages in chat.

%prep
%autosetup -n %{name}-%{commit}

%build
#gcc -Wall -fPIC nickchange.c -o nickchange.so -shared `pkg-config --cflags --libs glib-2.0 purple pidgin`
gcc %{optflags} -fPIC nickchange.c -o nickchange.so -shared `pkg-config --cflags --libs glib-2.0 purple pidgin`

%install
mkdir -p %{buildroot}%{_libdir}/purple-2
install -m0755 nickchange.so %{buildroot}%{_libdir}/purple-2/nickchange.so

%files
%license LICENSE
%doc README.md
%{_libdir}/purple-2/nickchange.so


%changelog
* Thu Dec 22 2016 Ken Dreyer <ktdreyer@ktdreyer.com> - 0-0.1.20160905gitc0b3e4a
- Initial package
